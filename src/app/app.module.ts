import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import{HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';
import { InventoryItemComponent } from './admin/inventory-item/inventory-item.component';
import { InventoryItemEditComponent } from './admin/inventory-item-edit/inventory-item-edit.component';
import { InventoryItemDetailsComponent } from './admin/inventory-item-details/inventory-item-details.component';
import { HeaderComponent } from './header/header.component';
import { InventoryListComponent } from './admin/inventory-list/inventory-list.component';
import { InvetoryStartComponent } from './admin/invetory-start/invetory-start.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    HomeComponent,
    InventoryItemComponent,
    InventoryItemEditComponent,
    InventoryItemDetailsComponent,
    HeaderComponent,
    InventoryListComponent,
    InvetoryStartComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
