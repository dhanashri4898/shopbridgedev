import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { InventoryItemDetailsComponent } from './admin/inventory-item-details/inventory-item-details.component';
import { InventoryItemEditComponent } from './admin/inventory-item-edit/inventory-item-edit.component';
import { InventoryListComponent } from './admin/inventory-list/inventory-list.component';
import { InvetoryStartComponent } from './admin/invetory-start/invetory-start.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'admin', component: AdminComponent,
    children: [
      { path:'',component:InvetoryStartComponent},
      { path: 'new', component: InventoryItemEditComponent },
      { path: ':id', component: InventoryItemDetailsComponent },
      { path: ':id/edit', component: InventoryItemEditComponent }
    ]
  },
  { path: '**', component: HomeComponent },
  {
    path: 'home',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
