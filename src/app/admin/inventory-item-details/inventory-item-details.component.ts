import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Inventory } from '../inventory.model';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-item-details',
  templateUrl: './inventory-item-details.component.html',
  styleUrls: ['./inventory-item-details.component.scss']
})
export class InventoryItemDetailsComponent implements OnInit {
  inventoryElement:Inventory=new Inventory("","",[]);
  id=0;
  constructor(
    private inventoryService: InventoryService,
              private route: ActivatedRoute,
              private routes:Router
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params :Params) =>{
         this.id = +params['id'];
        
        this.inventoryElement = this.inventoryService.getInventorybyId(this.id);
      });
  }
  onEditInventory(){
    this.routes.navigate(['edit'],{relativeTo: this.route});
  }
  onDeleteInventory(){
    this.inventoryService.onDelete(this.id);
    this.routes.navigate(['/admin']);
  }

}
