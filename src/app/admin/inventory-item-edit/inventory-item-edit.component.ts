import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Inventory } from '../inventory.model';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-item-edit',
  templateUrl: './inventory-item-edit.component.html',
  styleUrls: ['./inventory-item-edit.component.scss']
})
export class InventoryItemEditComponent implements OnInit {

  id: number = 0;

  editmode = false;
  inventoryForm: FormGroup = new FormGroup({});

  constructor(private route: ActivatedRoute,
    private invService: InventoryService,
    private router: Router) { }


  ngOnInit(): void {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editmode = params['id'] != null;
        console.log(this.editmode);
        this.initForm();
      }
    );

  }


  private initForm() {
    let inventoryName = "";

    let inventoryDiscription = "";
    let Items = new FormArray([]);

    if (this.editmode) {
      const Inventory = this.invService.getInventorybyId(this.id);
      inventoryName = Inventory.name;

      inventoryDiscription = Inventory.description;
      if(Inventory['Items']){ 
        for(let item of Inventory.Items){
          Items.push(
            new FormGroup({
             'itemname' : new FormControl(item.itemname),
             'amount':new FormControl(item.amount),
             'price':new FormControl(item.price)
            })
  
          );
        }
        
      }



    }
    this.inventoryForm = new FormGroup({
      name: new FormControl(inventoryName, Validators.required),

      discription: new FormControl(inventoryDiscription, Validators.required),
      Items:Items

    });

  }
  onSubmit() {
    const newInventory = new Inventory(
      this.inventoryForm.value['name'],
      this.inventoryForm.value['discription'],
      this.inventoryForm.value["Items"]

    );
    if (this.editmode) {
      this.invService.updateInventory(this.id, newInventory);

    } else {
      this.invService.addInventory(newInventory);

    }
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }
  onDeleteItem(index: number) {
    (<FormArray>this.inventoryForm.get('Items')).removeAt(index);
  }

  getControls() {
    return (this.inventoryForm.get('Items') as FormArray).controls;
  }

  addItem() {
    (<FormArray>this.inventoryForm.get('Items')).push(
      new FormGroup({
        'itemname': new FormControl(null, Validators.required),

        'amount': new FormControl(null, [Validators.required, Validators.pattern("^[1-9]+[0-9]*$")]),
        'price': new FormControl(null, [Validators.required, Validators.pattern("^[1-9]+[0-9]*$")])
      })

    );

  }
}
