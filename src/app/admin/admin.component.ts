import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Inventory } from './inventory.model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public inventoryList:Inventory[]=[];
  constructor(private router:Router) { }

  ngOnInit() {
    
  }
  onAddNew(){
    this.router.navigate(["admin","new"]);
  }
}
