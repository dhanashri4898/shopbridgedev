import { Injectable, EventEmitter } from "@angular/core";
import { } from "events";
import { Subject } from "rxjs";
import { Inventory } from "./inventory.model";
import { Item } from "./item.model";

@Injectable(
    { providedIn: 'root' }
)
export class InventoryService {
    constructor() {

    }
    inventories: Inventory[] = [
        new Inventory("Inventory 1", "Description", [
            new Item("Item1", 10, 15)
        ]),
        new Inventory("Inventory 2", "Description", [
            new Item("Item1", 10, 15),
            new Item("Item2", 9, 15)
        ])
    ];
    inventorySelected = new EventEmitter<Inventory>();
    inventoryChanged = new Subject<Inventory[]>();

    getList() {
        return this.inventories.slice();
    }
    getInventorybyId(index: number) {
        return this.inventories[index];
    }
    addInventory(Inventory: Inventory) {
        this.inventories.push(Inventory);
        this.inventoryChanged.next(this.inventories.slice());
    }
    updateInventory(index: number, newInventory: Inventory) {
        this.inventories[index] = newInventory;
        this.inventoryChanged.next(this.inventories.slice());

    }

    onDelete(index: number) {
        this.inventories.splice(index, 1);
        this.inventoryChanged.next(this.inventories.slice());

    }
}