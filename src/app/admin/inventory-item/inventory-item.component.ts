import { Component, Input, OnInit } from '@angular/core';
import { Inventory } from '../inventory.model';

@Component({
  selector: 'app-inventory-item',
  templateUrl: './inventory-item.component.html',
  styleUrls: ['./inventory-item.component.scss']
})
export class InventoryItemComponent implements OnInit {

  @Input('invItem') invItem:Inventory;
  @Input('index') index;
  constructor() { }

  ngOnInit() {
  }

}
