import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvetoryStartComponent } from './invetory-start.component';

describe('InvetoryStartComponent', () => {
  let component: InvetoryStartComponent;
  let fixture: ComponentFixture<InvetoryStartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvetoryStartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvetoryStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
