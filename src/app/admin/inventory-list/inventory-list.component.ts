import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Inventory } from '../inventory.model';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss']
})
export class InventoryListComponent implements OnInit {

  private InvSubscripton : any;
  public inventoryList:Inventory[]=[];
  constructor(private router:Router,private route:ActivatedRoute,private inventoryService:InventoryService) { }

  ngOnInit() {
    this.InvSubscripton= this.inventoryService.inventoryChanged.subscribe(
      (invs :Inventory[]) => {
        this.inventoryList = invs;
      }
      
    );
    this.inventoryList = this.inventoryService.getList();
  }
  onAddNew(){
    this.router.navigate(['new'], {relativeTo:this.route});
  }
}
