import { Item } from "./item.model";
export class Inventory{
    public name : string;
    public description : string;
   
    public Items : Item[];
    constructor(name: string,desc:string,Item : Item[]){
        this.name=name;
        this.description=desc;
       this.Items=Item;
    }
}